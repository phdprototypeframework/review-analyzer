SHELL := /bin/bash

.PHONY: build
build:
	tox -e build-dists

.PHONY: test
test:
	tox

.PHONY: check
check:
	tox -e checkqa

.PHONY: requirements
requirements:
	pip-compile --generate-hashes --allow-unsafe --output-file requirements.txt

.PHONY: upgrade-requirements
upgrade-requirements:
	pip-compile --upgrade --generate-hashes --allow-unsafe --output-file requirements.txt

.PHONY: sync-requirements
sync-requirements:
	pip-sync requirements-dev.txt
