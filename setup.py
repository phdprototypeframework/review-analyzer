from setuptools import find_packages, setup

setup(
    name="review-analyzer",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="NLP analysis of reviews",
    author="Tinashe Wamambo",
    packages=find_packages(exclude=["tests"]),
    python_requires=">=3.6",
    install_requires=["markdown", "textblob~=0.15", "nltk~=3.3"],
)
