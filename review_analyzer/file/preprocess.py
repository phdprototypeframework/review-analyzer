from nltk.corpus import stopwords


def remove_stopwords(text):
    """ Remove stopwords from text.

    :return string
    """
    words = []
    stopwordslist = set(stopwords.words("english"))

    for word in text.split(" "):
        if word not in stopwordslist:
            words.append(word)

    return " ".join(words)
