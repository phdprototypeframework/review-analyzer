class Sentence:
    def __init__(self, sentence):
        """ Constructor

        :return void
        """
        self.sentence = sentence

    def __str__(self):
        """ Return string representation of the
        sentence.

        :return string
        """
        return str(self.sentence)

    @property
    def sentiment(self):
        """ Retrieve sentiment which contains
        polarity and subjectivity.

        :return tuple
        """
        return self.sentence.sentiment

    @property
    def polarity(self):
        """ Return a string representation of the
        polarity.

        :return string
        """
        if self.sentence.sentiment.polarity >= 0.2:
            return "positive"

        if self.sentence.sentiment.polarity <= -0.2:
            return "negative"

        return "neutral"

    @property
    def subjective(self):
        """ Determine whether opinion expressed in sentence is
        subjective or objective.

        :return boolean
        """
        if self.sentence.sentiment.subjectivity > 0.5:
            return True

        return False

    @property
    def noun_phrases(self):
        """ Extract noun phrases from
        words in sentence.

        :return list
        """
        return self.sentence.noun_phrases

    @property
    def pos_tags(self):
        """ Extract Parts-of-Speech tagging
        from words in sentence.

        :return list
        """
        return self.sentence.tags

    @property
    def features(self):
        """ Retrieve features; commonly words with noun related
        pos tags or noun phrases from words in sentence.

        :return list
        """
        features = []
        for word, tag in self.pos_tags:
            if tag not in ["NN", "NNP", "NNS", "VB", "VBP", "VBG", "VBN"]:
                continue

            if self.noun_phrases:
                for noun_phrase in self.noun_phrases:
                    if word == noun_phrase or word in noun_phrase:
                        features.append(noun_phrase)
                    else:
                        features.append(word)
            else:
                features.append(word)

        return list(set(features))
