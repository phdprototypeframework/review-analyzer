from textblob import TextBlob, Word

from .preprocess import remove_stopwords
from .sentence import Sentence


class Review:
    def __init__(
        self, review, preprocess=False, lemmatize=False, spelling_correction=False
    ):
        """ Constructor

        :return void
        """
        self.original_text = review
        self.analysed_text = self.create_textblob(preprocess, lemmatize)

        if spelling_correction:
            self.analysed_text = self.spelling_correction

    def __str__(self):
        """ Return string representation of the
        original review text.

        :return string
        """
        return self.original_text

    def create_textblob(self, preprocess, lemmatize):
        """ Decide whether to preprocess review or not.

        :return TextBlob
        """
        if preprocess and lemmatize:
            text = remove_stopwords(self.original_text.lower())
            return TextBlob(" ".join(self.lemmatize(text.split(" "))))

        if preprocess:
            return TextBlob(remove_stopwords(self.original_text.lower()))

        if lemmatize:
            return TextBlob(
                " ".join(self.lemmatize(self.original_text.lower().split(" ")))
            )

        return TextBlob(self.original_text.lower())

    def lemmatize(self, words):
        """ Lemmatize words
        """
        return [Word(word).lemmatize("v") for word in words]

    @property
    def sentiment(self):
        """ Retrieve sentiment which contains
        polarity and subjectivity.

        :return tuple
        """
        return self.analysed_text.sentiment

    @property
    def polarity(self):
        """ Retrieve polarity of review.

        :return float
        """
        return self.analysed_text.sentiment.polarity

    @property
    def pos_tags(self):
        """ Extract Parts-of-Speech tagging
        from words in review.

        :return list
        """
        return self.analysed_text.tags

    @property
    def noun_phrases(self):
        """ Extract noun phrases from
        words in review.

        :return list
        """
        return self.analysed_text.noun_phrases

    @property
    def sentences(self):
        """ Extract sentences from review.

        :yield TextBlob.Sentence
        """
        for sentence in self.analysed_text.sentences:
            yield Sentence(sentence)

    @property
    def words(self):
        """ Extract words from review.

        :yield TextBlob.Word
        """
        for word in self.analysed_text.words:
            yield word

    @property
    def spelling_correction(self):
        """ Correct spelling mistakes in review.

        :return TextBlob
        """
        return self.analysed_text.correct()

    @property
    def features(self):
        """ Retrieve features; commonly words with noun related
        pos tags or noun phrases from words in review.

        :return list
        """
        features = []
        for word, tag in self.pos_tags:
            if tag not in ["NN", "NNP", "NNS", "VB", "VBP", "VBG", "VBN"]:
                continue

            features.append(word)

        return list(set(features))
