# Review Analyzer

This package handles the sentiment analysis of reviews using NLP and the TextBlob library.

## Usage

Coming soon...

## Sentiment

The sentiment property returns a namedtuple of the form `Sentiment(polarity, subjectivity)`.

The polarity score is a float within the range `[-1.0, 1.0]`.

The subjectivity is a float within the range `[0.0, 1.0]` where `0.0` is very objective and `1.0` is very subjective.
